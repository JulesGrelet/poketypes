using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class startLevel : MonoBehaviour
{

    public static List<int> unlockedLevels = new List<int>();
    public List<List<int>> paramLevels = new List<List<int>>();
    public static int levelsNb;
    public static int goalScore;
    public static int resetNb;
    public static int level;

    // Start is called before the first frame update
    void Start() {

        // number of levels
        levelsNb = 9;

        //level1
            List<int> l1 = new List<int>();
            l1.Add(1000); l1.Add(20); // goalScore = 1000, resetNb = 20
        //level2
            List<int> l2 = new List<int>();
            l2.Add(1250); l2.Add(20);
        //level3
            List<int> l3 = new List<int>();
            l3.Add(1500); l3.Add(20);
        //level4
            List<int> l4 = new List<int>();
            l4.Add(1500); l4.Add(15);
        //level5
            List<int> l5 = new List<int>();
            l5.Add(1750); l5.Add(15);
        //level6
            List<int> l6 = new List<int>();
            l6.Add(2000); l6.Add(15);
        //level7
            List<int> l7 = new List<int>();
            l7.Add(2000); l7.Add(10);
        //level8
            List<int> l8 = new List<int>();
            l8.Add(2250); l8.Add(10);
        //level9
            List<int> l9 = new List<int>();
            l9.Add(2500); l9.Add(10);

        //all levels
        paramLevels.Add(l1); paramLevels.Add(l2); paramLevels.Add(l3);
        paramLevels.Add(l4); paramLevels.Add(l5); paramLevels.Add(l6);
        paramLevels.Add(l7); paramLevels.Add(l8); paramLevels.Add(l9);

        // déblocage de niveau(x)
        GameObject levels = GameObject.FindGameObjectWithTag("Levels");
            // au démarage
            if (unlockedLevels.Count==0)
                unlockedLevels.Add(1);
            else if (unlockedLevels.Count>1) {

                for(int i=1; i<unlockedLevels.Count; i++) {
                    Transform level = levels.transform.GetChild(i);
                    level.gameObject.GetComponent<Image>().color = new Color(255, 255, 255);
                    level.gameObject.GetComponent<Button>().enabled = true;
                }

            }

        // drawing dotted line beetween level buttons
        //Handles.DrawLine(levels.transform.GetChild(0).position, levels.transform.GetChild(1).position);

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void loadLevel(int levelNumber) {

        // set the value of static variables
        goalScore = paramLevels[levelNumber-1][0];
        resetNb = paramLevels[levelNumber-1][1];
        level = levelNumber;

        // load level scene
        SceneManager.LoadScene(1);

    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeSceneFlowers : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        int flowersNumber = 10;

        List<int> tallHerbs = new List<int>();
        tallHerbs.Add(28); tallHerbs.Add(30); tallHerbs.Add(32); tallHerbs.Add(34); tallHerbs.Add(36);
        tallHerbs.Add(54); tallHerbs.Add(56); tallHerbs.Add(58); tallHerbs.Add(60); tallHerbs.Add(62);
        tallHerbs.Add(80); tallHerbs.Add(82); tallHerbs.Add(84); tallHerbs.Add(86); tallHerbs.Add(88);
        tallHerbs.Add(106); tallHerbs.Add(108); tallHerbs.Add(110); tallHerbs.Add(112); tallHerbs.Add(114);
        tallHerbs.Add(132); tallHerbs.Add(134); tallHerbs.Add(136); tallHerbs.Add(138); tallHerbs.Add(140);
        tallHerbs.Add(158); tallHerbs.Add(160); tallHerbs.Add(162); tallHerbs.Add(164); tallHerbs.Add(166);
        tallHerbs.Add(184); tallHerbs.Add(186); tallHerbs.Add(188); tallHerbs.Add(190); tallHerbs.Add(192);
        tallHerbs.Add(210); tallHerbs.Add(212); tallHerbs.Add(214); tallHerbs.Add(216); tallHerbs.Add(218);

        List<int> borderHerbs = new List<int>();
        borderHerbs.Add(25); borderHerbs.Add(26);
        borderHerbs.Add(38); borderHerbs.Add(39);
        borderHerbs.Add(51); borderHerbs.Add(52);
        borderHerbs.Add(64); borderHerbs.Add(65);
        borderHerbs.Add(77); borderHerbs.Add(78);
        borderHerbs.Add(90); borderHerbs.Add(91);
        borderHerbs.Add(103); borderHerbs.Add(104);
        borderHerbs.Add(116); borderHerbs.Add(117);
        borderHerbs.Add(129); borderHerbs.Add(130);
        borderHerbs.Add(142); borderHerbs.Add(143);
        borderHerbs.Add(155); borderHerbs.Add(156);
        borderHerbs.Add(168); borderHerbs.Add(169);
        borderHerbs.Add(181); borderHerbs.Add(182);
        borderHerbs.Add(194); borderHerbs.Add(195);
        borderHerbs.Add(207); borderHerbs.Add(208);
        borderHerbs.Add(220); borderHerbs.Add(221);

        GameObject herbs = GameObject.FindGameObjectWithTag("herbs_light");
        GameObject flowers = GameObject.FindGameObjectWithTag("flowers_light");

        List<int> availableHerbs = new List<int>();
        for (int i=14; i<=232; i++) {
            if(!tallHerbs.Contains(i) && !borderHerbs.Contains(i))
                availableHerbs.Add(i);
        }

        List<int> flowersCases = new List<int>();
        for (int i=0; i<=flowersNumber-1; i++) {

            int n = Random.Range(14, 232);
            if(availableHerbs.Contains(n) && !flowersCases.Contains(n)) {
                Transform flower = flowers.transform.GetChild(i);
                Transform herbsItem = herbs.transform.GetChild(n);
                flower.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
                flowersCases.Add(n);
            } else {
                i--;
            }
            
        }
                
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

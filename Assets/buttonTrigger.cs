using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class buttonTrigger : MonoBehaviour {

    public void resetTypes() {
        
        GameObject herbs = GameObject.FindGameObjectWithTag("herbs_tall_light");
        GameObject types = GameObject.FindGameObjectWithTag("types");

        // update remaining resets
        GameObject reset = GameObject.FindGameObjectWithTag("RemainingResetValue");
        reset.GetComponent<Text>().text = (int.Parse(reset.GetComponent<Text>().text)-1).ToString();
        if (int.Parse(reset.GetComponent<Text>().text)<0)
            reset.GetComponent<Text>().text = "0";

        // types position reset
        for (int x=0; x<=17; x++) {
            for (int y=0; y<=39; y++) {
                Transform type = types.transform.GetChild(x).GetChild(y);
                type.transform.position = new Vector3(-2.43f, -0.004f, 12);
            }
        }

        // red bordering reset (types alignments)
        GameObject typesRedBorders = GameObject.FindGameObjectWithTag("red_border");
        for (int x=0; x<=39; x++) {
            Transform redBorderItem = typesRedBorders.transform.GetChild(x);
            redBorderItem.position = new Vector3(-2.43f, -0.004f, 12);
            redBorderItem.GetChild(0).GetComponent<Text>().text="";
        }

        // new types
        for (int i=0; i<=39; i++) {
            int n = Random.Range(0, 17);
            Transform herbs_item = herbs.transform.GetChild(i);
            Transform type_acier = types.transform.GetChild(n).GetChild(i);
            type_acier.transform.position = herbs_item.transform.position;
        }

        // reset and new def types
        GameObject defTypes = GameObject.FindGameObjectWithTag("def_type");
        for (int j=0; j<=17; j++) {
            Transform defTypeReset = defTypes.transform.GetChild(j);
            defTypeReset.gameObject.SetActive(false);
        }
        int nNew = Random.Range(0, 17);
        Transform defTypeNew = defTypes.transform.GetChild(nNew);
        defTypeNew.gameObject.SetActive(true);

        // types verification
        List<Transform> boardTypes = new List<Transform>();
        for (int i=0; i<=39; i++) { // for each tall herbs
            for (int x=0; x<=17; x++) { // for each types
                for (int y=0; y<=39; y++) { // for each types item

                    Transform type = types.transform.GetChild(x).GetChild(y);
                    Transform herbs_item = herbs.transform.GetChild(i);

                    if (type.position==herbs_item.position)
                        boardTypes.Add(type);
                    
                }
            }
        }

        // board types COLUMNS (8)
        // (de gauche a droite 0 a 7 / de bas en haut a l'interieur des listes)
        List<Transform> col0 = new List<Transform>();
            col0.Add(boardTypes[0]); col0.Add(boardTypes[1]); col0.Add(boardTypes[2]); col0.Add(boardTypes[3]); col0.Add(boardTypes[4]);
        List<Transform> col1 = new List<Transform>();
            col1.Add(boardTypes[5]); col1.Add(boardTypes[6]); col1.Add(boardTypes[7]); col1.Add(boardTypes[8]); col1.Add(boardTypes[9]);
        List<Transform> col2 = new List<Transform>();
            col2.Add(boardTypes[10]); col2.Add(boardTypes[11]); col2.Add(boardTypes[12]); col2.Add(boardTypes[13]); col2.Add(boardTypes[14]);
        List<Transform> col3 = new List<Transform>();
            col3.Add(boardTypes[15]); col3.Add(boardTypes[16]); col3.Add(boardTypes[17]); col3.Add(boardTypes[18]); col3.Add(boardTypes[19]);
        List<Transform> col4 = new List<Transform>();
            col4.Add(boardTypes[20]); col4.Add(boardTypes[21]); col4.Add(boardTypes[22]); col4.Add(boardTypes[23]); col4.Add(boardTypes[24]);
        List<Transform> col5 = new List<Transform>();
            col5.Add(boardTypes[25]); col5.Add(boardTypes[26]); col5.Add(boardTypes[27]); col5.Add(boardTypes[28]); col5.Add(boardTypes[29]);
        List<Transform> col6 = new List<Transform>();
            col6.Add(boardTypes[30]); col6.Add(boardTypes[31]); col6.Add(boardTypes[32]); col6.Add(boardTypes[33]); col6.Add(boardTypes[34]);
        List<Transform> col7 = new List<Transform>();
            col7.Add(boardTypes[35]); col7.Add(boardTypes[36]); col7.Add(boardTypes[37]); col7.Add(boardTypes[38]); col7.Add(boardTypes[39]);

        List<List<Transform>> cols = new List<List<Transform>>();
            cols.Add(col0); cols.Add(col1); cols.Add(col2); cols.Add(col3); cols.Add(col4); cols.Add(col5); cols.Add(col6); cols.Add(col7);

        // board types LINES (5)
        // (de bas en haut 0 a 4 / de gauche a droite a l'interieur des listes)
        List<Transform> line0 = new List<Transform>();
            line0.Add(boardTypes[0]); line0.Add(boardTypes[5]); line0.Add(boardTypes[10]); line0.Add(boardTypes[15]);
            line0.Add(boardTypes[20]); line0.Add(boardTypes[25]); line0.Add(boardTypes[30]); line0.Add(boardTypes[35]);
        List<Transform> line1 = new List<Transform>();
            line1.Add(boardTypes[1]); line1.Add(boardTypes[6]); line1.Add(boardTypes[11]); line1.Add(boardTypes[16]);
            line1.Add(boardTypes[21]); line1.Add(boardTypes[26]); line1.Add(boardTypes[31]); line1.Add(boardTypes[36]);
        List<Transform> line2 = new List<Transform>();
            line2.Add(boardTypes[2]); line2.Add(boardTypes[7]); line2.Add(boardTypes[12]); line2.Add(boardTypes[17]);
            line2.Add(boardTypes[22]); line2.Add(boardTypes[27]); line2.Add(boardTypes[32]); line2.Add(boardTypes[37]);
        List<Transform> line3 = new List<Transform>();
            line3.Add(boardTypes[3]); line3.Add(boardTypes[8]); line3.Add(boardTypes[13]); line3.Add(boardTypes[18]);
            line3.Add(boardTypes[23]); line3.Add(boardTypes[28]); line3.Add(boardTypes[33]); line3.Add(boardTypes[38]);
        List<Transform> line4 = new List<Transform>();
            line4.Add(boardTypes[4]); line4.Add(boardTypes[9]); line4.Add(boardTypes[14]); line4.Add(boardTypes[19]);
            line4.Add(boardTypes[24]); line4.Add(boardTypes[29]); line4.Add(boardTypes[34]); line4.Add(boardTypes[39]);

        List<List<Transform>> lines = new List<List<Transform>>();
            lines.Add(line0); lines.Add(line1); lines.Add(line2); lines.Add(line3); lines.Add(line4);


        // verif types
        List<List<Transform>> founded_types = new List<List<Transform>>();

        List<Transform> founded_types_cols = new List<Transform>();
        for (int col=0; col<=7; col++) {
        
            for (int a=0; a<=4; a++) {
                for (int b=0; b<=4; b++) {
                    if (a!=b && !founded_types_cols.Contains(cols[col][a]) && !founded_types_cols.Contains(cols[col][b])) {

                        List<Transform> founded = new List<Transform>();
                        
                        for (int c=0; c<=4; c++) {
                            if (a!=c && b!=c && !founded_types_cols.Contains(cols[col][c])) {

                                if (cols[col][a].parent.name == cols[col][b].parent.name && cols[col][b].parent.name == cols[col][c].parent.name) {

                                    if (!founded_types_cols.Contains(cols[col][a]))
                                        founded_types_cols.Add(cols[col][a]);
                                    if (!founded_types_cols.Contains(cols[col][b]))
                                        founded_types_cols.Add(cols[col][b]);
                                    if (!founded_types_cols.Contains(cols[col][c]))
                                        founded_types_cols.Add(cols[col][c]);

                                    if (!founded.Contains(cols[col][a]))
                                        founded.Add(cols[col][a]);
                                    if (!founded.Contains(cols[col][b]))
                                        founded.Add(cols[col][b]);
                                    if (!founded.Contains(cols[col][c]))
                                        founded.Add(cols[col][c]);
                                        
                                }

                            }
                        }

                        if (founded.Count>0)
                            founded_types.Add(founded);

                    }
                }
            }

        }

        List<Transform> founded_types_lines = new List<Transform>();
        for (int line=0; line<=4; line++) {
        
            for (int a=0; a<=7; a++) {
                for (int b=0; b<=7; b++) {
                    if (a!=b && !founded_types_lines.Contains(lines[line][a]) && !founded_types_lines.Contains(lines[line][b])) {

                        List<Transform> founded = new List<Transform>();

                        for (int c=0; c<=7; c++) {
                            if (a!=c && b!=c && !founded_types_lines.Contains(lines[line][c])) {

                                if (lines[line][a].parent.name == lines[line][b].parent.name && lines[line][b].parent.name == lines[line][c].parent.name) {
                                    
                                    if (!founded_types_lines.Contains(lines[line][a]))
                                        founded_types_lines.Add(lines[line][a]);
                                    if (!founded_types_lines.Contains(lines[line][b]))
                                        founded_types_lines.Add(lines[line][b]);
                                    if (!founded_types_lines.Contains(lines[line][c]))
                                        founded_types_lines.Add(lines[line][c]);

                                    if (!founded.Contains(lines[line][a]))
                                        founded.Add(lines[line][a]);
                                    if (!founded.Contains(lines[line][b]))
                                        founded.Add(lines[line][b]);
                                    if (!founded.Contains(lines[line][c]))
                                        founded.Add(lines[line][c]);
                                    
                                }

                            }
                        }

                        if (founded.Count>0)
                            founded_types.Add(founded);

                    }
                }
            }

        }

        // red bordering of types alignments
        GameObject redBorders = GameObject.FindGameObjectWithTag("red_border");
        List<Transform> bordering_types = new List<Transform>();
        for (int i=0; i<founded_types.Count; i++) {
            for (int j=0; j<founded_types[i].Count; j++) {
                if (!bordering_types.Contains(founded_types[i][j])) {
                    Transform redBorderItem = redBorders.transform.GetChild(bordering_types.Count);
                    redBorderItem.position = founded_types[i][j].position;
                    bordering_types.Add(founded_types[i][j]);
                }
            }
        }

        // types sensibilites
        List<string> allDefTypes = new List<string>();
        allDefTypes.Add("type_acier_def"); allDefTypes.Add("type_combat_def"); allDefTypes.Add("type_dragon_def");
        allDefTypes.Add("type_eau_def"); allDefTypes.Add("type_electrik_def"); allDefTypes.Add("type_fee_def");
        allDefTypes.Add("type_feu_def"); allDefTypes.Add("type_glace_def"); allDefTypes.Add("type_insecte_def");
        allDefTypes.Add("type_normal_def"); allDefTypes.Add("type_plante_def"); allDefTypes.Add("type_poison_def");
        allDefTypes.Add("type_psy_def"); allDefTypes.Add("type_roche_def"); allDefTypes.Add("type_sol_def");
        allDefTypes.Add("type_spectre_def"); allDefTypes.Add("type_tenebre_def"); allDefTypes.Add("type_vol_def");

        List<string> allTypes = new List<string>();
        allTypes.Add("type_acier"); allTypes.Add("type_combat"); allTypes.Add("type_dragon"); allTypes.Add("type_eau"); allTypes.Add("type_electrik");
        allTypes.Add("type_fee"); allTypes.Add("type_feu"); allTypes.Add("type_glace"); allTypes.Add("type_insecte"); allTypes.Add("type_normal");
        allTypes.Add("type_plante"); allTypes.Add("type_poison"); allTypes.Add("type_psy"); allTypes.Add("type_roche"); allTypes.Add("type_sol");
        allTypes.Add("type_spectre"); allTypes.Add("type_tenebre"); allTypes.Add("type_vol");

        List<List<float>> allSensib = new List<List<float>>();
            List<float> sensib1 = new List<float>();
                sensib1.Add(0.5f); sensib1.Add(2f); sensib1.Add(0.5f); sensib1.Add(1f); sensib1.Add(1f); sensib1.Add(0.5f); sensib1.Add(2f); sensib1.Add(0.5f); sensib1.Add(0.5f);
                sensib1.Add(0.5f); sensib1.Add(0.5f); sensib1.Add(0f); sensib1.Add(0.5f); sensib1.Add(0.5f); sensib1.Add(2f); sensib1.Add(1f); sensib1.Add(1f); sensib1.Add(0.5f);
            List<float> sensib2 = new List<float>();
                sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(2f); sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(0.5f);
                sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(2f); sensib2.Add(0.5f); sensib2.Add(1f); sensib2.Add(1f); sensib2.Add(0.5f); sensib2.Add(2f);
            List<float> sensib3 = new List<float>();
                sensib3.Add(1f); sensib3.Add(1f); sensib3.Add(2f); sensib3.Add(0.5f); sensib3.Add(0.5f); sensib3.Add(2f); sensib3.Add(0.5f); sensib3.Add(2f); sensib3.Add(1f);
                sensib3.Add(1f); sensib3.Add(0.5f); sensib3.Add(1f); sensib3.Add(1f); sensib3.Add(1f); sensib3.Add(1f); sensib3.Add(1f); sensib3.Add(1f); sensib3.Add(1f);
            List<float> sensib4 = new List<float>();
                sensib4.Add(0.5f); sensib4.Add(1f); sensib4.Add(1f); sensib4.Add(0.5f); sensib4.Add(2f); sensib4.Add(1f); sensib4.Add(0.5f); sensib4.Add(0.5f); sensib4.Add(1f);
                sensib4.Add(1f); sensib4.Add(2f); sensib4.Add(1f); sensib4.Add(1f); sensib4.Add(1f); sensib4.Add(1f); sensib4.Add(1f); sensib4.Add(1f); sensib4.Add(1f);
            List<float> sensib5 = new List<float>();
                sensib5.Add(0.5f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(0.5f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(1f);
                sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(2f); sensib5.Add(1f); sensib5.Add(1f); sensib5.Add(0.5f);
            List<float> sensib6 = new List<float>();
                sensib6.Add(2f); sensib6.Add(0.5f); sensib6.Add(0f); sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(0.5f);
                sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(2f); sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(1f); sensib6.Add(0.5f); sensib6.Add(1f);
            List<float> sensib7 = new List<float>();
                sensib7.Add(0.5f); sensib7.Add(1f); sensib7.Add(1f); sensib7.Add(2f); sensib7.Add(1f); sensib7.Add(0.5f); sensib7.Add(0.5f); sensib7.Add(0.5f);sensib7.Add(0.5f);
                sensib7.Add(1f); sensib7.Add(0.5f); sensib7.Add(1f); sensib7.Add(1f); sensib7.Add(2f); sensib7.Add(2f); sensib7.Add(1f); sensib7.Add(1f); sensib7.Add(1f);
            List<float> sensib8 = new List<float>();
                sensib8.Add(2f); sensib8.Add(2f); sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(2f); sensib8.Add(0.5f); sensib8.Add(1f);
                sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(2f); sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(1f); sensib8.Add(1f);
            List<float> sensib9 = new List<float>();
                sensib9.Add(1f); sensib9.Add(0.5f); sensib9.Add(1f); sensib9.Add(1f); sensib9.Add(1f); sensib9.Add(1f); sensib9.Add(2f); sensib9.Add(1f); sensib9.Add(1f);
                sensib9.Add(1f); sensib9.Add(0.5f); sensib9.Add(1f); sensib9.Add(1f); sensib9.Add(2f); sensib9.Add(0.5f); sensib9.Add(1f); sensib9.Add(1f); sensib9.Add(2f);
            List<float> sensib10 = new List<float>();
                sensib10.Add(1f); sensib10.Add(2f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f);
                sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(1f); sensib10.Add(0f); sensib10.Add(1f); sensib10.Add(1f);
            List<float> sensib11 = new List<float>();
                sensib11.Add(1f); sensib11.Add(1f); sensib11.Add(1f); sensib11.Add(0.5f); sensib11.Add(0.5f); sensib11.Add(1f); sensib11.Add(2f); sensib11.Add(2f); sensib11.Add(2f);
                sensib11.Add(1f); sensib11.Add(0.5f); sensib11.Add(2f); sensib11.Add(1f); sensib11.Add(1f); sensib11.Add(0.5f); sensib11.Add(1f); sensib11.Add(1f); sensib11.Add(2f);
            List<float> sensib12 = new List<float>();
                sensib12.Add(1f); sensib12.Add(0.5f); sensib12.Add(1f); sensib12.Add(1f); sensib12.Add(1f); sensib12.Add(0.5f); sensib12.Add(1f); sensib12.Add(1f); sensib12.Add(0.5f);
                sensib12.Add(1f); sensib12.Add(0.5f); sensib12.Add(0.5f); sensib12.Add(2f); sensib12.Add(1f); sensib12.Add(2f); sensib12.Add(1f); sensib12.Add(1f); sensib12.Add(1f);
            List<float> sensib13 = new List<float>();
                sensib13.Add(1f); sensib13.Add(0.5f); sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(2f);
                sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(0.5f); sensib13.Add(1f); sensib13.Add(1f); sensib13.Add(2f); sensib13.Add(2f); sensib13.Add(1f);
            List<float> sensib14 = new List<float>();
                sensib14.Add(2f); sensib14.Add(2f); sensib14.Add(1f); sensib14.Add(2f); sensib14.Add(1f); sensib14.Add(1f); sensib14.Add(0.5f); sensib14.Add(1f); sensib14.Add(1f);
                sensib14.Add(0.5f); sensib14.Add(2f); sensib14.Add(0.5f); sensib14.Add(1f); sensib14.Add(1f); sensib14.Add(2f); sensib14.Add(1f); sensib14.Add(1f); sensib14.Add(0.5f);
            List<float> sensib15 = new List<float>();
                sensib15.Add(1f); sensib15.Add(1f); sensib15.Add(1f); sensib15.Add(2f); sensib15.Add(0f); sensib15.Add(1f); sensib15.Add(1f); sensib15.Add(2f); sensib15.Add(1f);
                sensib15.Add(1f); sensib15.Add(2f); sensib15.Add(0.5f); sensib15.Add(1f); sensib15.Add(0.5f); sensib15.Add(1f); sensib15.Add(1f); sensib15.Add(1f); sensib15.Add(1f);
            List<float> sensib16 = new List<float>();
                sensib16.Add(1f); sensib16.Add(0f); sensib16.Add(1f); sensib16.Add(1f); sensib16.Add(1f); sensib16.Add(1f); sensib16.Add(1f); sensib16.Add(1f); sensib16.Add(0.5f);
                sensib16.Add(0f); sensib16.Add(1f); sensib16.Add(0.5f); sensib16.Add(1f); sensib16.Add(1f); sensib16.Add(1f); sensib16.Add(2f); sensib16.Add(2f); sensib16.Add(1f);
            List<float> sensib17 = new List<float>();
                sensib17.Add(1f); sensib17.Add(2f); sensib17.Add(1f); sensib17.Add(1f); sensib17.Add(1f); sensib17.Add(2f); sensib17.Add(1f); sensib17.Add(1f); sensib17.Add(2f);
                sensib17.Add(1f); sensib17.Add(1f); sensib17.Add(1f); sensib17.Add(0f); sensib17.Add(1f); sensib17.Add(1f); sensib17.Add(0.5f); sensib17.Add(0.5f); sensib17.Add(1f);
            List<float> sensib18 = new List<float>();
                sensib18.Add(1f); sensib18.Add(0.5f); sensib18.Add(1f); sensib18.Add(1f); sensib18.Add(2f); sensib18.Add(1f); sensib18.Add(1f); sensib18.Add(2f); sensib18.Add(0.5f);
                sensib18.Add(1f); sensib18.Add(0.5f); sensib18.Add(1f); sensib18.Add(1f); sensib18.Add(2f); sensib18.Add(0f); sensib18.Add(1f); sensib18.Add(1f); sensib18.Add(1f);
        allSensib.Add(sensib1); allSensib.Add(sensib2); allSensib.Add(sensib3); allSensib.Add(sensib4); allSensib.Add(sensib5); allSensib.Add(sensib6);
        allSensib.Add(sensib7); allSensib.Add(sensib8); allSensib.Add(sensib9); allSensib.Add(sensib10); allSensib.Add(sensib11); allSensib.Add(sensib12);
        allSensib.Add(sensib13); allSensib.Add(sensib14); allSensib.Add(sensib15); allSensib.Add(sensib16); allSensib.Add(sensib17); allSensib.Add(sensib18);

        // get defensive type
        string defType = "";
        int defTypeIndex = 0;
        for (int i=0; i<=17; i++) {
            if (defTypes.transform.GetChild(i).gameObject.activeSelf) {
                defType = defTypes.transform.GetChild(i).gameObject.name;
                defTypeIndex = i;
            }
        }

        // calculation and score update
        GameObject score = GameObject.FindGameObjectWithTag("scoreValue");
        for (int n=0; n<founded_types.Count; n++) {

            int typeIndex = allTypes.IndexOf(founded_types[n][0].parent.name);
            float sensibValue = allSensib[defTypeIndex][typeIndex];

            int current_score = int.Parse(score.GetComponent<Text>().text);
            int new_score = current_score + (int)(100*(founded_types[n].Count-2)*sensibValue);
            score.GetComponent<Text>().text = new_score.ToString();

        }
        if(int.Parse(score.GetComponent<Text>().text)>999999)
            score.GetComponent<Text>().text = (999999).ToString();

        // score verification (win/lost)
        GameObject goalScore = GameObject.FindGameObjectWithTag("goalScoreValue");
        string goalScoreStr = goalScore.GetComponent<Text>().text;
        int goalScoreInt = int.Parse(goalScoreStr);

        GameObject remainReset = GameObject.FindGameObjectWithTag("RemainingResetValue");
        string remainResetStr = remainReset.GetComponent<Text>().text;
        int remainResetInt = int.Parse(remainResetStr);

        GameObject currScore = GameObject.FindGameObjectWithTag("scoreValue");
        string currScoreStr = currScore.GetComponent<Text>().text;
        int currScoreInt = int.Parse(currScoreStr);

        if (currScoreInt>=goalScoreInt) {
            //victoire

            if (startLevel.unlockedLevels.Contains(startLevel.level+1)==false && startLevel.level<startLevel.levelsNb)
                startLevel.unlockedLevels.Add(startLevel.level+1);

            setEndLevelVariables.score = currScoreInt;
            setEndLevelVariables.goalScore = goalScoreInt;
            setEndLevelVariables.remainResets = remainResetInt;

            SceneManager.LoadScene(2);

        } else if (remainResetInt==0) {
            //défaite

            setEndLevelVariables.score = currScoreInt;
            setEndLevelVariables.goalScore = goalScoreInt;
            setEndLevelVariables.remainResets = remainResetInt;

            SceneManager.LoadScene(3);
                
        }

    }

    public void retry() {
        SceneManager.LoadScene(1);
    }

    public void close() {
        SceneManager.LoadScene(0);
    }

}

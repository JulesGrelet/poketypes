using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeSceneTallHerbs : MonoBehaviour
{
    
    // Start is called before the first frame update
    void Start()
    {

        List<int> herbsCases = new List<int>();
        herbsCases.Add(28); herbsCases.Add(30); herbsCases.Add(32); herbsCases.Add(34); herbsCases.Add(36);
        herbsCases.Add(54); herbsCases.Add(56); herbsCases.Add(58); herbsCases.Add(60); herbsCases.Add(62);
        herbsCases.Add(80); herbsCases.Add(82); herbsCases.Add(84); herbsCases.Add(86); herbsCases.Add(88);
        herbsCases.Add(106); herbsCases.Add(108); herbsCases.Add(110); herbsCases.Add(112); herbsCases.Add(114);
        herbsCases.Add(132); herbsCases.Add(134); herbsCases.Add(136); herbsCases.Add(138); herbsCases.Add(140);
        herbsCases.Add(158); herbsCases.Add(160); herbsCases.Add(162); herbsCases.Add(164); herbsCases.Add(166);
        herbsCases.Add(184); herbsCases.Add(186); herbsCases.Add(188); herbsCases.Add(190); herbsCases.Add(192);
        herbsCases.Add(210); herbsCases.Add(212); herbsCases.Add(214); herbsCases.Add(216); herbsCases.Add(218);

        GameObject herbs = GameObject.FindGameObjectWithTag("herbs_light");
        GameObject tallHerbs = GameObject.FindGameObjectWithTag("herbs_tall_light");

        for(int i=0; i<=39; i++) {
            Transform tallHerb = tallHerbs.transform.GetChild(i);
            Transform herbsItem = herbs.transform.GetChild(herbsCases[i]);
            tallHerb.transform.position = herbsItem.transform.position;
            herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

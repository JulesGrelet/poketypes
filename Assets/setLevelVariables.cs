using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class setLevelVariables : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        GameObject goalScore = GameObject.FindGameObjectWithTag("goalScoreValue");
        GameObject reset = GameObject.FindGameObjectWithTag("RemainingResetValue");

        if (startLevel.goalScore>0)
            goalScore.GetComponent<Text>().text = startLevel.goalScore.ToString();
        else
            goalScore.GetComponent<Text>().text = "1000";

        if (startLevel.resetNb>0)
            reset.GetComponent<Text>().text = startLevel.resetNb.ToString();
        else
            reset.GetComponent<Text>().text = "20";
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

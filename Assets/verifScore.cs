using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class verifScore : MonoBehaviour
{
    // Start is called before the first frame update
    void Start() {

        GameObject goalScore = GameObject.FindGameObjectWithTag("goalScoreValue");
        string goalScoreStr = goalScore.GetComponent<Text>().text;
        int goalScoreInt = int.Parse(goalScoreStr);

        GameObject remainReset = GameObject.FindGameObjectWithTag("RemainingResetValue");
        string remainResetStr = remainReset.GetComponent<Text>().text;
        int remainResetInt = int.Parse(remainResetStr);

        GameObject currScore = GameObject.FindGameObjectWithTag("scoreValue");
        string currScoreStr = currScore.GetComponent<Text>().text;
        int currScoreInt = int.Parse(currScoreStr);

        if (currScoreInt>=goalScoreInt) {
            //victoire

            if (startLevel.unlockedLevels.Contains(startLevel.level+1)==false && startLevel.level<startLevel.levelsNb)
                startLevel.unlockedLevels.Add(startLevel.level+1);

            setEndLevelVariables.score = currScoreInt;
            setEndLevelVariables.goalScore = goalScoreInt;
            setEndLevelVariables.remainResets = remainResetInt;

            SceneManager.LoadScene(2);

        } else if (remainResetInt==0) {
            //défaite

            setEndLevelVariables.score = currScoreInt;
            setEndLevelVariables.goalScore = goalScoreInt;
            setEndLevelVariables.remainResets = remainResetInt;

            SceneManager.LoadScene(3);
                
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

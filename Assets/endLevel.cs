using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class endLevel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

        GameObject score = GameObject.FindGameObjectWithTag("scoreValue");
        GameObject goalScore = GameObject.FindGameObjectWithTag("goalScoreValue");
        GameObject remainResets = GameObject.FindGameObjectWithTag("RemainingResetValue");
        GameObject levelNumber = GameObject.FindGameObjectWithTag("ResultText");

        score.GetComponent<Text>().text = setEndLevelVariables.score.ToString();
        goalScore.GetComponent<Text>().text = setEndLevelVariables.goalScore.ToString();
        remainResets.GetComponent<Text>().text = setEndLevelVariables.remainResets.ToString();
        if (SceneManager.GetActiveScene().buildIndex==2)
            levelNumber.GetComponent<Text>().text = "Niveau "+startLevel.level.ToString()+"\n\nVictoire !";
        else if (SceneManager.GetActiveScene().buildIndex==3)
            levelNumber.GetComponent<Text>().text = "Niveau "+startLevel.level.ToString()+"\n\nDéfaite !";
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

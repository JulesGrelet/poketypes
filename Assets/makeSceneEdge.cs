using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class makeSceneEdge : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {

        GameObject herbs = GameObject.FindGameObjectWithTag("herbs_light");

        GameObject leftEdge = GameObject.FindGameObjectWithTag("left_edge");
        GameObject rightEdge = GameObject.FindGameObjectWithTag("right_edge");
        GameObject topEdge = GameObject.FindGameObjectWithTag("top_edge");
        GameObject bottomEdge = GameObject.FindGameObjectWithTag("bottom_edge");
        
        GameObject rockGreyLight = GameObject.FindGameObjectWithTag("rock_grey_light");
        
        // left side
        for(int i=0; i<=11; i++) {
            if (i==0) {
                Transform rock = rockGreyLight.transform.GetChild(0);
                Transform herbsItem = herbs.transform.GetChild(i);
                rock.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            } else {
                Transform edge = leftEdge.transform.GetChild(i-1);
                Transform herbsItem = herbs.transform.GetChild(i);
                edge.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            }
        }

        // right side
        for(int i=1; i<=12; i++) {
            if (i==12) {
                Transform rock = rockGreyLight.transform.GetChild(1);
                Transform herbsItem = herbs.transform.GetChild(298);
                rock.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            } else {
                Transform edge = rightEdge.transform.GetChild(i-1);
                Transform herbsItem = herbs.transform.GetChild(i+286);
                edge.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            }
        }

        // top side
        for(int i=0; i<=21; i++) {
            if (i==0) {
                Transform rock = rockGreyLight.transform.GetChild(2);
                Transform herbsItem = herbs.transform.GetChild(12);
                rock.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            } else {
                Transform edge = topEdge.transform.GetChild(i-1);
                Transform herbsItem = herbs.transform.GetChild(i+(i+1)*12);
                edge.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            }
        }

        // bottom side
        for(int i=1; i<=22; i++) {
            if (i==22) {
                Transform rock = rockGreyLight.transform.GetChild(3);
                Transform herbsItem = herbs.transform.GetChild(286);
                rock.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            } else {
                Transform edge = bottomEdge.transform.GetChild(i-1);
                Transform herbsItem = herbs.transform.GetChild(i*13);
                edge.transform.position = herbsItem.transform.position;
                herbsItem.transform.position = new Vector3(-2.43f, -0.004f, 12);
            }
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

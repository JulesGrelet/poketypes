using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class displayDefType : MonoBehaviour
{
    // Start is called before the first frame update
    void Start() {

        GameObject defTypes = GameObject.FindGameObjectWithTag("def_type");
        int n = Random.Range(0, 17);
        Transform defType = defTypes.transform.GetChild(n);
        defType.gameObject.SetActive(true);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class displayTypes : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
        GameObject herbs = GameObject.FindGameObjectWithTag("herbs_tall_light");
        GameObject types = GameObject.FindGameObjectWithTag("types");

        for (int i=0; i<=39; i++) {
            int n = Random.Range(0, 17);
            Transform herbs_item = herbs.transform.GetChild(i);
            Transform type = types.transform.GetChild(n).GetChild(i);
            type.transform.position = herbs_item.transform.position;
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
